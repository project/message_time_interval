MESSAGE TIME INTERVAL
------------


INTRODUCTION
------------
	This module hides drupal messages after a certain time interval.

REQUIREMENTS
------------

INSTALLATION
------------


CONFIGURATION
-------------
	Goto: /admin/config/user-interface/message-time-interval
	- Option is provided for enabled or disbaled the message hide functioanlity


MAINTAINERS
-----------
Supporting by :
	https://www.drupal.org/u/ankitjaince
